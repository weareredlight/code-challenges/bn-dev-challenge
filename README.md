# Brands&Ninjas Internship 2023 - Dev Challenge

# 

In this challenge we expect you to **implement** a web platform to create drinks for a bar 🍷. 

#

### Goals

This web platform should allow the user to:

- Create new drinks
- List existing drinks
- Show an existing drink
- Update existing drinks

### Phases

##### The frontend

On the frontend phase we want to see web pages were you can complete the goals established before. That is, create, list, show or update drinks.

Feel free to use any CSS frameworks like **Tailwind**, **Bootstrap**, or any similar one if your are familiar with it. If you want a challenge you can also try to finish this step using any web framework such as **React**, **Angular** or **VueJS**.

##### The backend

For the backend you should develop a server that responds to the frontend requests and integrates with a database that stores the information about the drinks.

Here you're also free to use any backend technology you're familiar with, be it **Ruby on Rails**, **Django**, **ExpressJS**, or any other of your choosing. For database technologies you can achieve this either using relational databases such as **PostgreSQL** and **MySQL** or by using non-relational databases such as **MongoDB**.

##### Some extras

Once the application allows the user to perform the main goals, you can also develop the following extras:

- Validate that the backend can only receive the parameters you want it to receive, no more, no less.
- Create validations for the form fields.
- Add an option to create multiple drinks at once.

##### Tips

- Use technologies you already feel confortable with.
- Take advantage of your strengths. If you feel that the backend is not going so well then focus more on the frontend and vice versa.

#

### Notes

A drink is composed of:

- name
- quantity

Example:

- name: Tequila
- quantity: 10

#

### Delivery
When you're done, you should fork this repository and upload your work there to share it with us or you can simply send everything in a .zip folder or a WeTransfer link.
Please try to share your process going through the steps you took to reach your final version (for example in the README).

